/*
 * Application.h
 *
 *  Created on: Feb 24, 2020
 *      Author: alexmorin
 */

#ifndef INC_APPLICATION_H_
#define INC_APPLICATION_H_
#ifdef __cplusplus
 extern "C" {
#endif

#include "CommManager.h"
#include "MotorManager.h"

#define HEARTBEAT_PERIOD 1000


 void ApplicationInit();
 void ApplicationTask();


#ifdef __cplusplus
}
#endif
#endif /* INC_APPLICATION_H_ */
