/*
 * MotorManager.h
 *
 *  Created on: Feb 24, 2020
 *      Author: alexmorin
 */


#ifndef MOTORMANAGER_H_
#define MOTORMANAGER_H_
#ifdef __cplusplus
 extern "C" {
#endif

#include "pca9685.h"
#include "stdbool.h"
#include "stdint.h"
#include "i2c.h"
#include "string.h"

 enum MotorEnum {
	 Motor0_1 = 0,
	 Motor2_3,
	 Motor4_5,
	 Motor14_15,
	 Motor10_11,
	 Motor12_13,
	 MaxMotor
 };

 enum ServoEnum {
	 Servo6 = 0,
	 Servo7,
	 Servo8,
	 Servo9,
	 MaxServo
 };

typedef struct {
	uint8_t u8Ch1;
    uint8_t u8Ch2;
    uint16_t u16Speed;
    uint8_t u8Direction;
    bool bHasChanged;
    uint32_t u32LastUpdate;
} oMotorDefinition_t, *poMotorDefinition_t;

typedef struct {
	uint8_t u8Ch;
    uint8_t u8Position;
    bool bHasChanged;
} oServoDefinition_t, *poServoDefinition_t;

 bool MotorManagerInit(I2C_HandleTypeDef *hi2c);
 bool MotorManagerTask();

 void MotorManagerSetMotorSpeed(enum MotorEnum motor, uint16_t u16Speed, uint8_t u8Speed);
 void MotorManagerSetMotorEnable(bool bEnabled);
 void MotorManagerSetServoPosition(enum ServoEnum servo, uint16_t u8Position);

#ifdef __cplusplus
}
#endif

#endif /* MOTORMANAGER_H_ */
