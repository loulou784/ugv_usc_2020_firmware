/*
 * MotorManager.c
 *
 *  Created on: Feb 24, 2020
 *      Author: alexmorin
 */

#include "MotorManager.h"

oMotorDefinition_t motorArray[MaxMotor];
oServoDefinition_t servoArray[MaxServo];


void MotorManagerMotorDefinitionInit(enum MotorEnum motor, uint8_t u8Ch1, uint8_t u8Ch2, uint16_t u16Speed, uint8_t u8Direction);
void MotorManagerServoDefinitionInit(enum ServoEnum servo, uint8_t u8Ch, uint8_t u8Position);

bool MotorManagerInit(I2C_HandleTypeDef *hi2c) {

	for(uint8_t i = 0; i < MaxMotor; i++) {
		memset(&motorArray[i], 0, sizeof(oMotorDefinition_t));
	}

	for(uint8_t i = 0; i < MaxServo; i++) {
		memset(&servoArray[i], 0, sizeof(oServoDefinition_t));
	}

	MotorManagerMotorDefinitionInit(Motor0_1,   0,  1,  0, 0);
	MotorManagerMotorDefinitionInit(Motor2_3,   2,  3,  0, 0);
	MotorManagerMotorDefinitionInit(Motor4_5,   4,  5,  0, 0);
	MotorManagerMotorDefinitionInit(Motor14_15, 14, 15, 0, 0);
	MotorManagerMotorDefinitionInit(Motor10_11, 10, 11, 0, 0);
	MotorManagerMotorDefinitionInit(Motor12_13, 12, 13, 0, 0);

	MotorManagerServoDefinitionInit(Servo6, 6, 0);
	MotorManagerServoDefinitionInit(Servo7, 7, 0);
	MotorManagerServoDefinitionInit(Servo8, 8, 0);
	MotorManagerServoDefinitionInit(Servo9, 9, 0);

	MotorManagerSetMotorEnable(true);

	return PCA9685_OK == PCA9685_Init(hi2c);
}

bool MotorManagerTask() {
	for(uint8_t i = 0; i < MaxMotor; i++) {
		if(motorArray[i].bHasChanged == true) {
			motorArray[i].bHasChanged = false;

			PCA9685_SetPin(motorArray[i].u8Ch1, (motorArray[i].u8Direction == 0) ? 0 : 4096, 0);
			PCA9685_SetPin(motorArray[i].u8Ch2, motorArray[i].u16Speed , 0);

		} else if((HAL_GetTick() - motorArray[i].u32LastUpdate) > 100) {
			MotorManagerSetMotorSpeed(i, 0, 0);
		}
	}

	for(uint8_t i = 0; i < MaxServo; i++) {
		if(servoArray[i].bHasChanged == true) {
			servoArray[i].bHasChanged = false;
			PCA9685_SetServoAngle(servoArray[i].u8Ch, servoArray[i].u8Position);
		}
	}
	return true;
}

void MotorManagerSetMotorEnable(bool bEnabled) {
	if(bEnabled == true) {
		HAL_GPIO_WritePin(PCA_ENABLE_GPIO_Port, PCA_ENABLE_Pin, GPIO_PIN_RESET);
	} else {
		HAL_GPIO_WritePin(PCA_ENABLE_GPIO_Port, PCA_ENABLE_Pin, GPIO_PIN_SET);
	}
}

void MotorManagerSetMotorSpeed(enum MotorEnum motor, uint16_t u16Speed, uint8_t u8Direction) {
	if(motor < MaxMotor) {
		if(u16Speed >= 4096) {
			motorArray[motor].u16Speed = 4096;
		} else if(u16Speed <= 0) {
			motorArray[motor].u16Speed = 0;
		} else {
			motorArray[motor].u16Speed = u16Speed;
		}

		if(u8Direction >= 1) {
			motorArray[motor].u8Direction = 1;
		} else if(u8Direction <= 0) {
			motorArray[motor].u8Direction = 0;
		}

		motorArray[motor].u32LastUpdate = HAL_GetTick();
		motorArray[motor].bHasChanged = true;
	}
}

void MotorManagerSetServoPosition(enum ServoEnum servo, uint16_t u8Position) {
	if(servo < MaxServo) {
		if(u8Position >= 180) {
			servoArray[servo].u8Position = 180;
		} else if(u8Position <= 0) {
			servoArray[servo].u8Position = 0;
		} else {
			servoArray[servo].u8Position = u8Position;
		}
		servoArray[servo].bHasChanged = true;
	}
}

//*** Private Function ***//
void MotorManagerMotorDefinitionInit(enum MotorEnum motor, uint8_t u8Ch1, uint8_t u8Ch2, uint16_t u16Speed, uint8_t u8Direction) {
	if(motor < MaxMotor) {
		motorArray[motor].u8Ch1 = u8Ch1;
		motorArray[motor].u8Ch2 = u8Ch2;
		motorArray[motor].u16Speed = u16Speed;
		motorArray[motor].u8Direction = u8Direction;
		motorArray[motor].bHasChanged = true;
	}
}

void MotorManagerServoDefinitionInit(enum ServoEnum servo, uint8_t u8Ch, uint8_t u8Position) {
	if(servo < MaxServo) {
		servoArray[servo].u8Ch = u8Ch;
		servoArray[servo].u8Position = u8Position;
		servoArray[servo].bHasChanged = true;
	}
}


