/*
 * Application.c
 *
 *  Created on: Feb 24, 2020
 *      Author: alexmorin
 */
#include "Application.h"

 uint16_t u16Length = 0;
 uint8_t u8RetVal = 0;
 uint8_t u8ReturnArray[256];

 uint32_t u32LastTickHeartBeat = 0;

 void ApplicationInit() {
	 CommManagerInit(&huart1);
	 MotorManagerInit(&hi2c3);
	 MotorManagerSetMotorEnable(true);
 }

 void ApplicationTask() {

	// Process DMA buffer char by char/*
	u8RetVal = CommManagerProcessBuffer(u8ReturnArray, &u16Length);

	if (u8RetVal) {
		//We have a valid packet in u8ReturnArray of size u16Length
		if (u8ReturnArray[2] == CMD_SPEED) {
			//Speed data of length 0x04
			if (u8ReturnArray[3] == 0x06) {
				oSpeedCommStruct_t oSpeeddata;
				memcpy(&oSpeeddata, &u8ReturnArray[4], sizeof(oSpeeddata));
				MotorManagerSetMotorSpeed(Motor0_1, oSpeeddata.u16LeftSpeed, oSpeeddata.u8LeftDir);
				MotorManagerSetMotorSpeed(Motor2_3, oSpeeddata.u16RightSpeed, oSpeeddata.u8RightDir);
			}
		} else if (u8ReturnArray[2] == CMD_SERVO6) {
			//Servo6 data of length 0x01
			if (u8ReturnArray[3] == 0x01) {
				MotorManagerSetServoPosition(Servo6, u8ReturnArray[4]);
			}
		}
	}

	MotorManagerSetMotorSpeed(Motor0_1, 0, 0);

	if ((HAL_GetTick() - u32LastTickHeartBeat) > HEARTBEAT_PERIOD) {
		CommManagerSendHeartBeat();
		HAL_GPIO_TogglePin(LD3_GPIO_Port, LD3_Pin);
		u32LastTickHeartBeat = HAL_GetTick();
	}


	MotorManagerTask();
 }
